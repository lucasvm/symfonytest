<?php
// src/Controller/ProductController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProductRepository;


class ProductController extends AbstractController
{

    #[Route('/product', name: 'add_product', methods: ['POST'])]
    public function addProduct(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator): Response {
        try {
            $product = $serializer->deserialize($request->getContent(), Product::class, 'json');
            $product->setCreatedAt(new \DateTimeImmutable()); // Manually set the date
            
             // Validate the product entity
            $errors = $validator->validate($product);
            if (count($errors) > 0) {
                return $this->json($errors, Response::HTTP_BAD_REQUEST);
            }

            $existingProduct = $entityManager->getRepository(Product::class)->findOneBy(['sku' => $product->getSku()]);
            
            if ($existingProduct) {
                // SKU already exists, return an error response
                return $this->json(['error' => 'SKU already exists.'], Response::HTTP_CONFLICT);
            }

            // Validate the product entity
            $errors = $validator->validate($product);
            if (count($errors) > 0) {
                return $this->json($errors, Response::HTTP_BAD_REQUEST);
            }

            $entityManager->persist($product);
            $entityManager->flush();

            $this->addFlash('success', 'New Product Added.');
            return $this->json(
                [
                    'success' => true,
                    'message' => 'Product successfully created.',
                    'data' => $product
                ],
                Response::HTTP_CREATED
            );
        } catch (\Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/product/{sku}', name: 'update_product', methods: ['PUT'])]
    public function updateProduct($sku, Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator): Response {
        $product = $entityManager->getRepository(Product::class)->findOneBy(['sku' => $sku]);

        if (!$product) {
            throw new NotFoundHttpException("Product with SKU $sku not found.");
        }

        try {
            $serializer->deserialize($request->getContent(), Product::class, 'json', ['object_to_populate' => $product]);

            // Validate the product entity
            $errors = $validator->validate($product);
            if (count($errors) > 0) {
                return $this->json($errors, Response::HTTP_BAD_REQUEST);
            }

            $entityManager->flush();

            return $this->json($product, Response::HTTP_OK);
        } catch (\Exception $e) {
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/product/{sku}', name: 'delete_product', methods: ['DELETE'])]
    public function deleteProduct(string $sku, ProductRepository $productRepository, EntityManagerInterface $entityManager): Response {
        $product = $productRepository->findOneBy(['sku' => $sku]);

        if (!$product) {
            return $this->json(['error' => 'Product not found.'], Response::HTTP_NOT_FOUND);
        }

        $entityManager->remove($product);
        $entityManager->flush();

        return $this->json(
            [
                'success' => true,
                'message' => 'Product successfully deleted.'
            ],
            Response::HTTP_OK
        );
    }

    #[Route('/products_get', name: 'list_products', methods: ['GET'])]
    public function listProducts(EntityManagerInterface $entityManager, SerializerInterface $serializer): Response {
        $products = $entityManager->getRepository(Product::class)->findAll();

        $jsonContent = $serializer->serialize($products, 'json');

        return new Response($jsonContent, Response::HTTP_OK, ['Content-Type' => 'application/json']);
    }

    #[Route('/products', name: 'products', methods: ['GET'])]
    public function list(ProductRepository $productRepository): Response
    {
        $products = $productRepository->findAll();

        return $this->render('product/list.html.twig', [
            'products' => $products,
        ]);
    }
}
