# SymfonyTest

## Description

Symfony App with endpoints to create, update and delete products via postman.

## Installation

### Requirements

- PHP (8+)
- Symfony (6)
- MySQL/PostgreSQL/other (Postgre)
- Composer

### Steps

1. **Clone the Repository**

Install Dependencies
composer install

**Configure Environment**

2. Copy the .env file to .env.local.
Modify .env.local to match your database and other environment settings.

**Database Setup**

3. Create a database for the application.

Run migrations to set up the database schema:


php bin/console doctrine:migrations:migrate

**Start the Server**


symfony server:start
Alternatively, if you're not using the Symfony binary:


php -S localhost:8000 -t public
API Endpoints
Replace [base-url] with the URL where the application is hosted (e.g., http://localhost:8000).

### Endpoints and how to:

Endpoint: [base-url]/products_get
Method: GET
Description: Retrieves a list of all products.

Endpoint: [base-url]/product
Method: POST
Description: Insert new product, using this payload:
{
    "sku": "123453",
    "productName": "Awesome Product",
    "description": "A detailed description of the awesome product."
}

Endpoint: [base-url]/product/{sku}
Method: PUT
Description: Update product using the following payload:
{
    "sku": "123453",
    "productName": "Awesome Product 2",
    "description": "A detailed description of the awesome product."
}

Endpoint: [base-url]/product/{sku}
Method: DELETE
Description: DELETE product using SKU

Web Access: [base-url]/products
Method: GET
Description: Retrieves a list of all products in a website view.